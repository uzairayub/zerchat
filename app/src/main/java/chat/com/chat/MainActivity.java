package chat.com.chat;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import chat.com.chat.Fragments.MessageFragment;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    public Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private SweetAlertDialog loadingdialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        loadingdialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("Connecting to chat");
        loadingdialog.show();
        sharedPreferences = getSharedPreferences("userinfo",MODE_PRIVATE);
        sendBird_init(sharedPreferences.getString("email","email"));

    }

    public void setFragment(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).addToBackStack(getTitle().toString()).commit();
    }
    public void sendBird_init(String id)
    {
        SendBird.init("71817001-DFB4-4D5E-982B-F041EFD604EC", MainActivity.this);
        SendBird.connect(id, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e == null)
                {
                    loadingdialog.dismiss();
                    setFragment(new MessageFragment(MainActivity.this));
                }
                else
                {

                }
            }
        });
    }

}
