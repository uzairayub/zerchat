package chat.com.chat.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import chat.com.chat.Adapters.ChatAdapter;
import chat.com.chat.MainActivity;
import chat.com.chat.Models.Model_Chat;
import chat.com.chat.R;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Muhammad Uzair Ayub on 7/13/2017.
 */

public class NewMessage extends Fragment implements View.OnClickListener {
    private MainActivity activity;
    private MessageFragment fragment;
    private GroupChannel mGroupChannel;
    private EditText txtemail, txtmessage;
    private ArrayList<String> users;
    private SharedPreferences sharedPreferences;
    private Button btnsend;
    private SweetAlertDialog loadingdialog, error;
    public NewMessage(MainActivity activity, MessageFragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        getActivity().setTitle("New Message");
        View view = inflater.inflate(R.layout.fragment_newmessage,container,false);
        sharedPreferences = getActivity().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        activity.toolbar.setNavigationIcon(R.drawable.back_arrow);
        activity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        txtemail = (EditText)view.findViewById(R.id.newmessage_et_email);
        txtmessage = (EditText)view.findViewById(R.id.newmessage_et_message);
        btnsend = (Button)view.findViewById(R.id.newmessage_btn_send);
        btnsend.setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View view)
    {
        if(!txtemail.getText().toString().isEmpty() && !txtmessage.getText().toString().isEmpty())
        {
            users = new ArrayList<>();
            String sender = sharedPreferences.getString("email",null);
            if(sender != null)
            {
                users.add(sender);
                users.add(txtemail.getText().toString());
                creatChannel(users);
                loadingdialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE).setTitleText("Sending message");
                loadingdialog.show();
            }
        }

    }

    void creatChannel (final ArrayList<String> users){
        mGroupChannel.createChannelWithUserIds(users, true, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                mGroupChannel = groupChannel;
                if (mGroupChannel != null)
                {
                    mGroupChannel.sendUserMessage(txtmessage.getText().toString(), new BaseChannel.SendUserMessageHandler() {
                        @Override
                        public void onSent(UserMessage userMessage, SendBirdException e)
                        {
                            if(e == null)
                            {
                                loadingdialog.dismiss();
                                getFragmentManager().popBackStack();
                                fragment.addGroupChannel();
                            }
                            else
                            {
                                loadingdialog.dismiss();
                                error = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE).setTitleText("User Not Found");
                                error.show();
                                txtemail.setText("");
                            }
                        }
                    });
                }
                else
                {
                    loadingdialog.dismiss();
                    error = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE).setTitleText("User Not Found");
                    error.show();
                    txtemail.setText("");
                }
            }
        });
    }


}
