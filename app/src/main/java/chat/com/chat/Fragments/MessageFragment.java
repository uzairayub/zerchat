package chat.com.chat.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelListQuery;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;

import java.util.ArrayList;
import java.util.List;

import chat.com.chat.Adapters.MessageAdapter;
import chat.com.chat.MainActivity;
import chat.com.chat.Models.Model_Messages;
import chat.com.chat.R;
import chat.com.chat.UserInfo;

public class MessageFragment extends Fragment {

    private GroupChannel available_group_list;
    private User user;
    private ArrayList<Model_Messages> mymessageslist;
    private RecyclerView recyclerView;
    private MainActivity activity;
    private SharedPreferences sharedPreferences;

    public MessageFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("My Chats");
        setHasOptionsMenu(true);
        sharedPreferences = getActivity().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        activity.toolbar.setNavigationIcon(null);
        View view = inflater.inflate(R.layout.fragment_message,container,false);
        recyclerView = (RecyclerView)view.findViewById(R.id.message_recylerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getgroupchannels();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main,menu );
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.mainmenu_userinfo)
        {
            startActivity(new Intent(getActivity(), UserInfo.class));
        }
        else if(item.getItemId() == R.id.mainmenu_newmessage)
        {
            activity.setFragment(new NewMessage(activity,this));
        }
        return super.onOptionsItemSelected(item);
    }

    void getgroupchannels()
    {
        GroupChannelListQuery channelListQuery = GroupChannel.createMyGroupChannelListQuery();
        channelListQuery.setIncludeEmpty(true);
        channelListQuery.next(new GroupChannelListQuery.GroupChannelListQueryResultHandler() {
            @Override
            public void onResult(List<GroupChannel> list, SendBirdException e) {
                if (e == null)
                {
                    mymessageslist = new ArrayList<Model_Messages>();
                    Model_Messages modelMessages;
                    for (int i = 0; i < list.size();i++ )
                    {
                        modelMessages = new Model_Messages();
                        available_group_list = list.get(i);
                        available_group_list.getUrl();
                        if(available_group_list.getMembers().size() == 2)
                        {
                            user = available_group_list.getMembers().get(0);
                            String currentuser = sharedPreferences.getString("email", "email");
                            if (user.getUserId().equals(currentuser)) {
                                user = available_group_list.getMembers().get(1);
                            }
                            UserMessage lastmessage = (UserMessage) available_group_list.getLastMessage();
                            modelMessages.setLastmessage(lastmessage.getMessage());
                            modelMessages.setUserdpurl(user.getProfileUrl());
                            modelMessages.setUsername((user.getNickname() == "") ? "NewUser" : user.getNickname());
                            modelMessages.setSender(sharedPreferences.getString("email", "email"));
                            modelMessages.setReciver(user.getUserId());
                            mymessageslist.add(modelMessages);
                        }
                    }
                    MessageAdapter adapter = new MessageAdapter(mymessageslist,getActivity());
                    recyclerView.setAdapter(adapter);
                }
            }
        });
    }
    void addGroupChannel()
    {
        getgroupchannels();

    }
}
