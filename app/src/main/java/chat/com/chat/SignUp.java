package chat.com.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.net.URL;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    private Button btnsignup;
    private EditText txtemail, txtpassword, txtcpassword, txtname;
    private FirebaseAuth mAuth;
    private SweetAlertDialog loadingdialog;
    private View myview;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sharedPreferences = getSharedPreferences("userinfo",MODE_PRIVATE);
        myview = (View)findViewById(R.id.signup_acitivity);
        btnsignup = (Button)findViewById(R.id.signup_btn_signup);
        btnsignup.setOnClickListener(this);
        txtemail = (EditText)findViewById(R.id.signup_et_email);
        txtname = (EditText)findViewById(R.id.signup_et_name);
        txtpassword = (EditText)findViewById(R.id.signup_et_pass);
        txtcpassword = (EditText)findViewById(R.id.signup_et_cpass);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.signup_btn_signup)
        {
            if(!txtemail.getText().toString().isEmpty() && !txtname.getText().toString().isEmpty() &&
                    !txtpassword.getText().toString().isEmpty() && !txtcpassword.getText().toString().isEmpty())
            {
                if (txtpassword.getText().toString().equals(txtpassword.getText().toString()))
                {
                    loadingdialog = new SweetAlertDialog(SignUp.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("Creating your new account");
                    loadingdialog.show();
                    signUp(txtemail.getText().toString(), txtcpassword.getText().toString());
                }
                else
                {
                    Snackbar.make(view,"Password didn't match",Snackbar.LENGTH_SHORT).show();
                    txtpassword.setText("");
                    txtpassword.setText("");
                }
            }
            else
            {
                Snackbar.make(myview,"Please enter valid information",Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    void signUp(String email, String password)
    {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            FirebaseUser user = mAuth.getCurrentUser();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email",user.getEmail());
                            editor.putString("name",txtname.getText().toString());
                            editor.commit();
                            loadingdialog.dismiss();
                            startActivity(new Intent(SignUp.this,MainActivity.class));
                            finish();

                        } else {
                            loadingdialog.dismiss();
                            Snackbar.make(myview,"SignUp failed please try again",Snackbar.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }
}
