package chat.com.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private Button btnlogin;
    private TextView txtsignup;
    private EditText txtemail, txtpassword;
    private FirebaseAuth mAuth;
    private View myview;
    private SharedPreferences sharedPreferences;
    private SweetAlertDialog loadingdialog;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = getSharedPreferences("userinfo",MODE_PRIVATE);
        myview = (View)findViewById(R.id.login_actvity);
        btnlogin = (Button)findViewById(R.id.login_btn_login);
        btnlogin.setOnClickListener(this);
        txtemail = (EditText)findViewById(R.id.login_et_email);
        txtpassword = (EditText)findViewById(R.id.login_et_pass);
        txtsignup = (TextView)findViewById(R.id.login_tv_signup);
        txtsignup.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.login_btn_login)
        {

            if(txtemail != null && txtpassword != null && !txtemail.getText().toString().isEmpty() && !txtpassword.getText().toString().isEmpty())
            {
                loadingdialog = new SweetAlertDialog(Login.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("Logging into your account");
                loadingdialog.show();
             login(txtemail.getText().toString(), txtpassword.getText().toString());
            }
            else
            {
                txtemail.setText("");
                txtpassword.setText("");
                Snackbar.make(myview,"Please enter valid email and password",Snackbar.LENGTH_SHORT).show();
            }
        }
        else if(view.getId() == R.id.login_tv_signup)
        {
            startActivity(new Intent(Login.this, SignUp.class));

        }
    }

    void login (String email, String password)
    {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            FirebaseUser user = mAuth.getCurrentUser();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email",user.getEmail());
                            editor.commit();
                            loadingdialog.dismiss();
                            startActivity(new Intent(Login.this, MainActivity.class));
                            finish();
                        }
                        else
                        {
                            loadingdialog.dismiss();
                            txtpassword.setText("");
                            Snackbar.make(myview,"Please enter valid email and password",Snackbar.LENGTH_LONG).show();
                        }
                    }
                });

    }
}
