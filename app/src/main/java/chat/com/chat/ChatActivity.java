package chat.com.chat;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import chat.com.chat.Adapters.ChatAdapter;
import chat.com.chat.Models.*;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private String chatsender = null;
    private String chatreciver = null;
    private ArrayList<String> users;
    private ArrayList<Model_Chat> mychatlist;
    private EditText txtmessage;
    private TextView btnsend;
    private GroupChannel mGroupChannel;
    private RecyclerView recyclerView;
    private ChatAdapter chatAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        txtmessage = (EditText)findViewById(R.id.chat_et_message);
        btnsend = (TextView)findViewById(R.id.chat_tv_send);
        Toolbar toolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        btnsend.setOnClickListener(this);
        users = new ArrayList<>();
        chatsender = getIntent().getExtras().getString("currentuser");
        chatreciver = getIntent().getExtras().getString("user");
        users = new ArrayList<>();
        users.add(chatsender);
        users.add(chatreciver);
        creatChannel(users);
        toolbar.setTitle(chatreciver);
        recyclerView = (RecyclerView) findViewById(R.id.chat_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.light_gray));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        toolbar.setNavigationOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    void creatChannel (final ArrayList<String> users){
        mGroupChannel.createChannelWithUserIds(users, true, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                mGroupChannel = groupChannel;
                if (mGroupChannel != null)
                {
                    onMessageReceive();
                    PreviousMessageListQuery prevMessageListQuery = groupChannel.createPreviousMessageListQuery();
                    prevMessageListQuery.load(30, true, new PreviousMessageListQuery.MessageListQueryResult() {
                        @Override
                        public void onResult(List<BaseMessage> messages, SendBirdException e) {
                            if (e == null) {
                                mychatlist = new ArrayList<>();
                                Model_Chat modelChat;
                                for (int i = 0; i < messages.size(); i++) {
                                    modelChat = new Model_Chat();
                                    UserMessage msg = (UserMessage) messages.get(i);
                                    modelChat.setMessage(msg.getMessage());
                                    modelChat.setTime(msg.getCreatedAt() + "");
                                    String ch = msg.getSender().getUserId();
                                    if (ch.equals(chatreciver)) {
                                        modelChat.setType(0);
                                    } else {
                                        modelChat.setType(1);
                                    }
                                    modelChat.setStstus(true);
                                    mychatlist.add(modelChat);
                                }
                                Collections.reverse(mychatlist);
                                chatAdapter = new ChatAdapter(mychatlist);
                                recyclerView.setAdapter(chatAdapter);
                                recyclerView.scrollToPosition(mychatlist.size() - 1);
                            } else {

                            }
                        }
                    });
                }
                else
                {
                    creatChannel(users);
                }
            }
        });
    }

    void onMessageReceive()
    {
        SendBird.addChannelHandler(String.valueOf(mGroupChannel), new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                if (baseMessage instanceof UserMessage)
                {
                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Model_Chat modelChat = new Model_Chat();
                    UserMessage  masg = (UserMessage)baseMessage;
                    modelChat.setMessage(masg.getMessage());
                    modelChat.setType(0);
                    mychatlist.add(modelChat);
                    chatAdapter.notifyDataSetChanged();
                    recyclerView.scrollToPosition(mychatlist.size()-1);
                }
                else if (baseMessage instanceof FileMessage) {
                    // message is a FileMessage
                } else if (baseMessage instanceof AdminMessage) {
                    // message is an AdminMessage
                }
            }
        });
    }
    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.chat_tv_send)
        {
            if(txtmessage != null && txtmessage.getText().toString().trim().length() > 0 && mGroupChannel != null)
            {
                Model_Chat modelChat = new Model_Chat();
                modelChat.setMessage(txtmessage.getText().toString());
                modelChat.setType(1);
                modelChat.setStstus(false);
                mychatlist.add(modelChat);
                chatAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(mychatlist.size()-1);
                mGroupChannel.sendUserMessage(txtmessage.getText().toString(), new BaseChannel.SendUserMessageHandler() {
                    @Override
                    public void onSent(UserMessage userMessage, SendBirdException e)
                    {
                        if(e == null)
                        {
                            Model_Chat current_chat = mychatlist.get(mychatlist.size()-1);
                            current_chat.setStstus(true);
                            chatAdapter.notifyDataSetChanged();
                        }
                        else
                        {

                        }
                    }
                });
                txtmessage.setText("");
            }
            else
            {

            }
        }
    }
}