package chat.com.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread thread = new Thread(){
            @Override
            public void run() {
                super.run();
                try {
                    Thread.sleep(1000);
                    sharedPreferences = getSharedPreferences("userinfo",MODE_PRIVATE);
                    String email = sharedPreferences.getString("email", null);
                    if(email == null)
                    {
                        startActivity(new Intent(Splash.this, Login.class));
                        finish();
                    }
                    else
                    {
                        startActivity(new Intent(Splash.this, MainActivity.class));
                        finish();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
