package chat.com.chat.Models;

/**
 * Created by Muhammad Uzair Ayub on 7/8/2017.
 */

public class Model_Messages
{
    private String userdpurl;
    private String  username;
    private String lastmessage;
    private String lastseen;
    private String sender;
    private String reciver;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReciver() {
        return reciver;
    }

    public void setReciver(String reciver) {
        this.reciver = reciver;
    }

    public String getUserdpurl() {
        return userdpurl;
    }

    public void setUserdpurl(String userdpurl) {
        this.userdpurl = userdpurl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public void setLastmessage(String lastmessage) {
        this.lastmessage = lastmessage;
    }

    public String getLastseen() {
        return lastseen;
    }

    public void setLastseen(String lastseen) {
        this.lastseen = lastseen;
    }
}
