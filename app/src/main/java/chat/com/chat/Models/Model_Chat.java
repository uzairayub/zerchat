package chat.com.chat.Models;

/**
 * Created by Muhammad Uzair Ayub on 7/7/2017.
 */

public class Model_Chat
{
    private String message;
    private String time;
    private boolean seenunseen;
    private int type;
    private String userdpurl;
    private Boolean ststus;

    public Boolean getStstus() {
        return ststus;
    }

    public void setStstus(Boolean ststus) {
        this.ststus = ststus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isSeenunseen() {
        return seenunseen;
    }

    public void setSeenunseen(boolean seenunseen) {
        this.seenunseen = seenunseen;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserdpurl() {
        return userdpurl;
    }

    public void setUserdpurl(String userdpurl) {
        this.userdpurl = userdpurl;
    }
}
