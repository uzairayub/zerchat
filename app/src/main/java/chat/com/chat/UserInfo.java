package chat.com.chat;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;

public class UserInfo extends AppCompatActivity implements View.OnClickListener {

    private ImageView userimage;
    private Button btnsave;
    private EditText txtname ;
    private TextView txtemail;
    private File file;
    private final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        sharedPreferences = getSharedPreferences("userinfo",MODE_PRIVATE);
        Toolbar toolbar = (Toolbar)findViewById(R.id.user_info_tolbar);
        toolbar.setTitle("User Information");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        userimage = (ImageView)findViewById(R.id.user_info_userimage);
        userimage.setOnClickListener(this);
        txtemail = (TextView) findViewById(R.id.user_info_email);
        txtemail.setText(sharedPreferences.getString("email","email"));
        txtname = (EditText)findViewById(R.id.user_info_name);
        btnsave = (Button)findViewById(R.id.user_info_button);
        btnsave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.user_info_button)
        {

            if(file != null)
            {
                if(!txtname.getText().toString().isEmpty())
                {
                    update(file,txtname.getText().toString());
                }
                else
                {
                    update(file,"newuser");
                }
            }
            else
            {
                File mfile = createFileFromResource(R.drawable.profile_placeholder,"file");
                if(!txtname.getText().toString().isEmpty())
                {
                    update(mfile,txtname.getText().toString());
                }
                else
                {
                    update(mfile,"newuser");
                }

            }
        }
        else if(view.getId() == R.id.user_info_userimage)
        {
            if(ContextCompat.checkSelfPermission(UserInfo.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                try
                {
                    startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"),2);
                }
                catch (android.content.ActivityNotFoundException ex)
                {
                }
            }
            else
            {
                getPermission();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == RESULT_OK)
        {
            Uri uri = data.getData();
            userimage.setImageURI(uri);
            try
            {
                String path =  getPath(this,uri);
                file = new File(path);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("path",path).commit();
                if(txtname.getText().toString().isEmpty())
                {
                    update(file,"newuser");
                }
                else
                {
                    update(file,txtname.getText().toString());
                }

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    void getPermission (){

        if (ActivityCompat.shouldShowRequestPermissionRationale(UserInfo.this, android.Manifest.permission.READ_EXTERNAL_STORAGE))
        {

        }
        else
        {
            ActivityCompat.requestPermissions(UserInfo.this,
            new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }

    }
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    void update(File file , String name)
    {
        SendBird.updateCurrentUserInfoWithProfileImage(name, file, new SendBird.UserInfoUpdateHandler() {
            @Override
            public void onUpdated(SendBirdException e) {
                Log.d("","");
            }
        });
    }

    public File createFileFromResource(int resId, String fileName)
    {
        File f;
        try
        {
            f = new File(getCacheDir() + File.separator + fileName);
            InputStream is = getResources().openRawResource(resId);
            OutputStream out = new FileOutputStream(f);

            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = is.read(buffer)) > 0)
            {
                out.write(buffer, 0, bytesRead);
            }

            out.close();
            is.close();
        }
        catch (IOException ex)
        {
            f = null;
        }
        return f;
    }
}
