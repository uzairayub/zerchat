package chat.com.chat.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import chat.com.chat.Models.Model_Chat;
import chat.com.chat.R;

/**
 * Created by waleed on 23/05/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    ArrayList<Model_Chat> list;

    public ChatAdapter(ArrayList<Model_Chat> list)
    {
        this.list = list;
    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType == 1)
        {
            View contactView = inflater.inflate(R.layout.chat_bubble_green, parent, false);
            ChatAdapter.ViewHolder viewHolder = new ChatAdapter.ViewHolder(contactView);
            return viewHolder;
        }
        else
        {
            View contactView = inflater.inflate(R.layout.chat_bubble_grey, parent, false);
            ChatAdapter.ViewHolder viewHolder = new ChatAdapter.ViewHolder(contactView);
            return viewHolder;
        }

    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.mesage.setText(list.get(position).getMessage());
        if(list.get(position).getType() == 1)
        {
            holder.status.setText(list.get(position).getStstus() ? "sent" : "sending");
        }
        else
        {
            holder.status.setText("");
        }
    }
    @Override
    public int getItemViewType(int position)
    {
        return list.get(position).getType() ;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mesage, status;
        public ViewHolder(View itemView) {
            super(itemView);
            mesage = (TextView)itemView.findViewById(R.id.chatbubble);
            status = (TextView)itemView.findViewById(R.id.chat_status);

        }
    }
}
