package chat.com.chat.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import chat.com.chat.ChatActivity;
import chat.com.chat.Models.Model_Messages;
import chat.com.chat.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by waleed on 16/05/2017.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    Context context;
    ArrayList<Model_Messages> list;

    public MessageAdapter(ArrayList<Model_Messages> list , Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.messages_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {

        holder.username.setText(list.get(position).getUsername());
        holder.lastmessage.setText(list.get(position).getLastmessage());
        if(list.get(position).getUserdpurl()!= null && list.get(position).getUserdpurl()!= "" && list.get(position).getUserdpurl()!= "null")
        {
            Picasso.with(context).load(list.get(position).getUserdpurl()).error(R.drawable.profile_placeholder).placeholder(R.drawable.profile_placeholder).into(holder.userimage);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MessageAdapter.this.context!=null){
                    Intent intent = new Intent(context,ChatActivity.class);
                    intent.putExtra("currentuser",list.get(position).getSender());
                    intent.putExtra("user",list.get(position).getReciver());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView userimage;
        TextView username, lastmessage;
        public ViewHolder(View itemView)
        {
            super(itemView);
            userimage = (CircleImageView)itemView.findViewById(R.id.message_imageview);
            username = (TextView)itemView.findViewById(R.id.message_tv_username);
            lastmessage = (TextView)itemView.findViewById(R.id.message_tv_lastmessage);
        }
    }


}
